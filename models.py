from flask import g
from sqlalchemy.orm import aliased
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from .index import db, app
from .config import *


relations = db.Table('relations',
    db.Column('user', db.Integer, db.ForeignKey('users.id')),
    db.Column('userTarget', db.Integer, db.ForeignKey('users.id'))
)


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True)
    password_hash = db.Column(db.String(128))
    type = db.Column(db.String(128), default="user")
    followed = db.relationship('User',
                               secondary=relations,
                               primaryjoin=(relations.c.user == id),
                               secondaryjoin=(relations.c.userTarget == id),
                               backref=db.backref('relations', lazy='dynamic'),
                               lazy='dynamic')

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=3600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    def follow(self, userToId):
        user_to = User.query.filter_by(id=userToId).first()
        if self.is_following(user_to):
            self.followed.remove(user_to)
        else:
            self.followed.append(user_to)
        return self

    def is_following(self, user_to):
        return self.followed.filter(relations.c.userTarget == user_to.id).count() > 0

    def is_admin(self):
        if self.type == ADMIN_TYPE:
            return True
        else:
            return False

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user

    @staticmethod
    def get_users():
        data = []
        connections = User.query.filter(User.relations.any(id=g.current_user.id)).all()
        filteredConnections = [connected.id for connected in connections]
        for user in User.query.all():
            if not user.is_admin() and user.username != g.current_user.username:
                dataUser = {}
                dataUser['id'] = user.id
                dataUser['username'] = user.username
                if user.id in filteredConnections:
                    dataUser['connected'] = True
                data.append(dataUser)
        return data

    @staticmethod
    def get_connections():
        u1 = aliased(User, name='u1')
        u2 = aliased(User, name='u2')
        connections = db.session.query(u1.username, u2.username).join(relations, (relations.c.user == u1.id)).join(u2, (relations.c.userTarget == u2.id)).all()
        formated_connections = [{'from': connection[0], 'to':connection[1]} for connection in connections]
        return formated_connections
