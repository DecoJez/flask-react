from .index import app
from .routes import index

app.debug = True

if __name__ == '__main__':
    app.run()