var webpack = require('webpack');
var path = require('path');

module.exports = {
  entry: [
    './app/app.js' // Your appʼs entry point
  ],
  devtool: process.env.WEBPACK_DEVTOOL || 'source-map',
  output: {
    path: path.join(__dirname, '../dist'),
    publicPath: '/scripts',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)?$/,
        exclude: /(node_modules)/,
        loaders: ['react-hot', 'babel', 'eslint'],
      },
      {
        test: /\.css?$/,
        loader: 'style!css?'
      },
      {
        test: /\.scss?$/,
        loader: 'style!css!sass?'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file"
      },
      {
        test: /\.(woff|woff2)(\?\S*)?$/,
        loader: "url?prefix=font/&limit=5000"
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=application/octet-stream"
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&mimetype=image/svg+xml"
      },
      {
        test: /\.gif/,
        loader: "url-loader?limit=10000&mimetype=image/gif"
      },
      {
        test: /\.jpg/,
        loader: "url-loader?limit=10000&mimetype=image/jpg"
      },
      {
        test: /\.png/,
        loader: "url-loader?limit=10000&mimetype=image/png"
      }
    ]
  },
  devServer: {
    contentBase: "./public",
      noInfo: true, //  --no-info option
      hot: true,
      //inline: true
  },
  plugins: [
    new webpack.ProvidePlugin({}),
    new webpack.NoErrorsPlugin()
  ]
};
