import {RECEIVE_USERS, REQUEST_USERS, RECEIVE_CONNECTIONS, REQUEST_CONNECTIONS} from '../constants/users';

const initialState = {
  users: [],
  connections: [],
  isFetching: false
};

export function users(state = initialState, action) {
  switch (action.type) {

    case REQUEST_USERS: {
      return Object.assign({}, state, {
        isFetching: true
      });
    }

    case RECEIVE_USERS: {
      return Object.assign({}, state, {
        isFetching: false,
        users: action.users
      });
    }

    case REQUEST_CONNECTIONS: {
      return Object.assign({}, state, {
        isFetching: true
      });
    }

    case RECEIVE_CONNECTIONS: {
      return Object.assign({}, state, {
        isFetching: false,
        connections: action.connections
      });
    }

    default:
      return state;
  }
}
