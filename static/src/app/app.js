import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, connect } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { Router, Route, IndexRoute, browserHistory} from 'react-router';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

import '../styles/main.scss'

import rootReducer from './reducers';

import { NoAuthRequired } from './containers/NoAuthRequired';

import Main from './containers/Main';
import Home from './containers/Home';
import Login from './containers/Login';
import Register from './containers/Register';
import List from './containers/List';
import Connections from './containers/Connections';

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);
const store = createStoreWithMiddleware(rootReducer);
const history = syncHistoryWithStore(browserHistory, store);


class Root extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Route path="/" component={Main}>
            <IndexRoute component={Home} />
            <Route path="login" component={NoAuthRequired(Login)} />
            <Route path="register" component={NoAuthRequired(Register)} />
            <Route path="list" component={List} />
            <Route path="connections" component={Connections} />
          </Route>
        </Router>
      </Provider>
    )
  }
}

function mapStateToProps(state) {
  return {
    routing: state.routing,
    auth: state.auth
  }
}

export default connect(mapStateToProps)(Root);

ReactDOM.render(<Root/>, document.getElementById('app'));
