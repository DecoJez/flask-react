import axios from 'axios';
import {RECEIVE_USERS, REQUEST_USERS, RECEIVE_CONNECTIONS, REQUEST_CONNECTIONS} from '../constants/users';

export function fetchUsers(token) {
    return dispatch => {
        dispatch(requestUsers());
        axios.get('/api/users', {
                headers: {
                    'Authorization': token // eslint-disable-line quote-props
                }
            })
            .then(response => dispatch(receiveUsers(response.data)))
    }
}

export function updateConnection(token, idTo) {
    return dispatch => {
        dispatch(requestUsers());
        axios({
                method: 'post',
                url: '/api/users/relation',
                headers: {
                    'Authorization': token // eslint-disable-line quote-props
                },
                data: {
                    idTo: idTo
                }
            })
            .then(response => dispatch(receiveUsers(response.data)))
    }
}

export function requestUsers() {
    return {
        type: REQUEST_USERS
    }
}

export function receiveUsers(data) {
    return {
        type: RECEIVE_USERS,
        users: data.users
    }
}

export function fetchConnections(token) {
    return dispatch => {
        dispatch(requestConnections());
        axios.get('/api/users/connections', {
                headers: {
                    'Authorization': token // eslint-disable-line quote-props
                }
            })
            .then(response => dispatch(receiveConnections(response.data)))
    }
}

export function requestConnections() {
    return {
        type: REQUEST_CONNECTIONS
    }
}

export function receiveConnections(data) {
    return {
        type: RECEIVE_CONNECTIONS,
        connections: data.connections
    }
}