import axios from 'axios';
import { RECEIVE_LOGIN, REQUEST_LOGIN, RECEIVE_REGISTER, REQUEST_REGISTER, LOGOUT} from '../constants/auth';

export function fetchLogin(username, password) {
  return dispatch => {
    dispatch(requestLogin());
    axios.post('api/login', {
        username,
        password
    })
      .then(response => dispatch(receiveLogin(response.data)))
  }
}

export function requestLogin() {
  return {
    type: REQUEST_LOGIN
  }
}

export function receiveLogin(data) {
  localStorage.setItem('token', data.token);
  return {
    type: RECEIVE_LOGIN,
    token: data.token,
    userId: data.id,
    isAdmin: data.userType === 'admin'
  }
}

export function logout() {
    localStorage.removeItem('token');
    return {
        type: LOGOUT
    };
}

export function fetchRegister(username, password) {
  return dispatch => {
    dispatch(requestRegister());
    axios.post('/api/register', {
        username,
        password
    })
      .then(response => dispatch(receiveRegister(response.data)))
  }
}

export function requestRegister() {
  return {
    type: REQUEST_REGISTER
  }
}

export function receiveRegister(data) {
  localStorage.setItem('token', data.token);
  return {
    type: RECEIVE_REGISTER,
    token: data.token,
    userId: data.id,
    isAdmin: data.userType === 'admin'
  }
}

