import React from 'react';
import { connect } from 'react-redux';

import * as usersActions from '../actions/usersActions';
import UserRow from '../components/UserRow';

import propTypes from '../constants/containersPropTypes';

class List extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(usersActions.fetchUsers(this.props.auth.token));
  }

  handleRowToggle(userTo) {
    if (this.props.auth.isAdmin !== true) {
      const { dispatch } = this.props;
      dispatch(usersActions.updateConnection(this.props.auth.token, userTo));
    }
  }

  render() {
    const {isFetching, users} = this.props.users;
    return (
      <section className="list">
        <ul className="collection">
        {!isFetching && (users.length > 0) ?
          users.map( user => <UserRow onClick={this.handleRowToggle.bind(this, user.id)} key={user.id} {...user}/>) :
          <p>Loading</p>
        }
        </ul>
      </section>
    )
  }
}

List.propTypes = propTypes;

const mapStateToProps = state => ({
    auth: state.auth,
    users: state.users
});

export default connect(mapStateToProps)(List);
