import React,  { PropTypes }  from 'react';
import { connect } from 'react-redux';

import * as usersActions from '../actions/usersActions';
import ConnectionRow from '../components/ConnectionRow';

import propTypes from '../constants/containersPropTypes';

class Connections extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(usersActions.fetchConnections(this.props.auth.token));
  }

  render() {
    const {isFetching, connections} = this.props.users;
    return (
      <section className="connections">
        <ul className="collection">
        {!isFetching && (connections.length > 0) ?
          connections.map( (connection, i) => <ConnectionRow key={i} {... connection}/>) :
          <p>Loading</p>
        }
        </ul>
      </section>
    )
  }
}

Connections.propTypes = propTypes;

const mapStateToProps = state => ({
    auth: state.auth,
    users: state.users
});


export default connect(mapStateToProps)(Connections);

