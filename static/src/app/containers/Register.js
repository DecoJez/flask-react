import React from 'react';
import { connect } from 'react-redux';

import * as authAction from '../actions/authActions';

import propTypes from '../constants/containersPropTypes';

class Register extends React.Component {

  constructor(props) {
    super(props);
  }

  handleRegister() {
    const { dispatch } = this.props;
    dispatch(authAction.fetchRegister(this.refs.registerUsername.value, this.refs.registerPassword.value));
  }

  render() {
    return (
      <section className="register">
        <input placeholder="Username" type="text" ref="registerUsername"/>
        <input placeholder="Password" type="password" ref="registerPassword"/>
        <button className="waves-effect waves-light btn" onClick={this.handleRegister.bind(this)}>Register</button>
      </section>
    )
  }
}

Register.propTypes = propTypes;

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(Register);
