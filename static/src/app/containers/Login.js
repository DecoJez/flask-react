import React from 'react';
import { connect } from 'react-redux';

import * as authAction from '../actions/authActions';

import propTypes from '../constants/containersPropTypes';

class Login extends React.Component {

  constructor(props) {
    super(props);
  }

  handleLogin() {
    const { dispatch } = this.props;
    dispatch(authAction.fetchLogin(this.refs.loginUsername.value, this.refs.loginPassword.value));
  }

  render() {
    return (
      <section className="login">
        <input placeholder="Username" type="text" ref="loginUsername"/>
        <input placeholder="Password" type="password" ref="loginPassword"/>
        <button className="waves-effect waves-light btn" onClick={this.handleLogin.bind(this)}>Login</button>
      </section>
    )
  }
}

Login.propTypes = propTypes;

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(Login);
