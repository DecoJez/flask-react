import React from 'react';

class UserRow extends React.Component {

  render() {
    return (
      <li className="collection-item">
          {this.props.from} -> {this.props.to}
      </li>
    )
  }
}

export default UserRow;