from functools import wraps
from flask import request, jsonify, g
from .models import User


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.headers.get('Authorization', None)
        if token:
            string_token = token.encode('ascii', 'ignore')
            user = User.verify_auth_token(string_token)
            if user:
                g.current_user = user
                return f(*args, **kwargs)
        return jsonify({'message': 'Please login on your account'}), 400
    return decorated


def requires_admin_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.headers.get('Authorization', None)
        if token:
            string_token = token.encode('ascii', 'ignore')
            user = User.verify_auth_token(string_token)
            if user and user.is_admin():
                g.current_user = user
                return f(*args, **kwargs)
        return jsonify({'message': 'Please login as admin'}), 400
    return decorated
