from flask import render_template, request, jsonify, g, redirect, url_for
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from .index import app, db
from .authentication import requires_auth, requires_admin_auth
from .models import User
from .config import *


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/<path:path>')
def redirect_path(path):
    return redirect(url_for('index'))


@app.route('/api/login', methods=['POST'])
def get_token():
    username = request.json.get('username')
    password = request.json.get('password')
    user = User.query.filter_by(username=username).first()
    if not user or not user.verify_password(password):
        return jsonify({'message': 'Incorrect credentials, try again'}), 400
    g.user = user
    token = g.user.generate_auth_token(TOKEN_EXPIRATION_TIME)
    return jsonify({'token': token.decode('ascii'), 'userId': user.id, 'userType': user.type})


@app.route('/api/register', methods=['POST'])
def new_user():
    username = request.json.get('username')
    password = request.json.get('password')
    if username is None or password is None:
        return jsonify({'message': 'Insert credentials'}), 400
    if User.query.filter_by(username=username).first() is not None:
        return jsonify({'message': 'User exist, choose other username'}), 400
    user = User(username=username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    g.user = user
    token = g.user.generate_auth_token(TOKEN_EXPIRATION_TIME)
    return jsonify({'token': token.decode('ascii'), 'userId': user.id, "userType": user.type}), 200


@app.route('/api/verify', methods=['POST'])
def verify_token():
    token = request.get_json()
    s = Serializer(app.config['SECRET_KEY'])
    try:
        user = s.loads(token['token'])
    except (BadSignature, SignatureExpired):
        return jsonify({'message': 'Token expired'}), 500
    typeOfUser = User.query.filter_by(id=user['id']).first().type
    return jsonify({'message': 'success', 'token': token["token"], 'userId': user['id'], 'userType': typeOfUser}), 200


@app.route('/api/users/relation', methods=['POST'])
@requires_auth
def new_follow():
    userToId = request.json.get('idTo')
    user = g.current_user
    relation = user.follow(userToId)
    db.session.add(relation)
    db.session.commit()
    data = User.get_users()
    return jsonify({'message': 'success', 'users': data})


@app.route('/api/users', methods=['GET'])
@requires_auth
def print_users():
    data = User.get_users()
    return jsonify({'message': 'success', "users": data}), 200


@app.route('/api/users/connections', methods=['GET'])
@requires_admin_auth
def print_connections():
    connections = User.get_connections()
    return jsonify({'message': 'success', 'connections': connections}), 200



